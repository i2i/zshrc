############################
# c.snizik zsh configuration
# updated 5/8/2019
############################

# Editor

export EDITOR=vim
export VISUAL=vim
export GREP_OPTIONS='--color=always'

HISTSIZE=10000
SAVEHIST=10000

# Aliases

alias ll="ls -alh"
alias fucking="sudo"
alias isaid="sudo"